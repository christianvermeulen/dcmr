<?php

namespace Rythm\DcmrBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ComplaintController extends Controller
{
    public function indexAction()
    {
        return $this->render('RythmDcmrBundle:Complaint:index.html.twig');
    }

}
