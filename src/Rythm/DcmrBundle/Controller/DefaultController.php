<?php

namespace Rythm\DcmrBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('RythmDcmrBundle:Pages:home.html.twig');
    }

    public function locationAction($city)
    {
        $location = str_replace("-", " ", $city);
        return $this->render('RythmDcmrBundle:Pages:location.html.twig', array("city" => $location));
    }

    public function aboutAction()
    {
        return $this->render('RythmDcmrBundle:Pages:about.html.twig');
    }

    public function faqAction()
    {
        return $this->render('RythmDcmrBundle:Pages:faq.html.twig');
    }

    public function contactAction()
    {
        return $this->render('RythmDcmrBundle:Pages:contact.html.twig');
    }
}
