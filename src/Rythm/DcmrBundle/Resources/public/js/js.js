$(document).ready(function(e){

    var prev;
    var next;
    var bar;
    var canvasHeight = $('#svg').height();
    var canvasWidth = $('#svg').width();
    var canvasWP= ($(window).width()/screen.width)-0.2;

     var moved = 0;
    var aantalDB;
    var soundImage;
    var image;
    var imageArray = ['iets','10','20','40','50','60','80','90','110','130','140'];
    var curImage = 4;
    var slider;
    var moved=0;

    var cursorX=0;
    var sliderX=370;
    var pointer;
    var textpoint;
    var rijnmond;
   $('#svg').svg({onLoad: drawIntro});

   //activate sound infographic
   $('#svg2').svg({onLoad: drawtable});


function drawIntro(svg){

   rijnmond = svg.group({id:'chart'});

   $(rijnmond).animate({svgTransform:"scale("+canvasWP+","+canvasWP+"),translate(0,150)"});
   //TODO alle urls vervangen
   var albrandswaard = svg.group(rijnmond,{class:'gemeente Albrandswaard'})
    svg.load('http://dcmr-site.hrprojecten.net/bundles/rythmdcmr/img/svg/albrandswaard.svg',{addTo:true,changeSize:false,parent:albrandswaard,svgX:500});
    $(albrandswaard).animate({svgTransform:"translate(590,115)"});

       var bernisse = svg.group(rijnmond,{class:'gemeente Bernisse'})
   svg.load('http://dcmr-site.hrprojecten.net/bundles/rythmdcmr/img/svg/bernisse.svg',{addTo:true,changeSize:false,parent:bernisse});
   $(bernisse).animate({svgTransform:"translate(304,209)"});

    var brielle = svg.group(rijnmond,{class:'gemeente Brielle'})
    svg.load('http://dcmr-site.hrprojecten.net/bundles/rythmdcmr/img/svg/brielle.svg',{addTo:true,changeSize:false,parent:brielle});
      $(brielle).animate({svgTransform:"translate(239,78)"});

    var barendrecht = svg.group(rijnmond,{class:'gemeente Barendrecht'})
    svg.load('http://dcmr-site.hrprojecten.net/bundles/rythmdcmr/img/svg/barendrecht.svg',{addTo:true,changeSize:false,parent:barendrecht});
    $(barendrecht).animate({svgTransform:"translate(690,164)"});

     var cappelle = svg.group(rijnmond,{class:'gemeente Capelle-aan-den-ijsel'})
    svg.load('http://dcmr-site.hrprojecten.net/bundles/rythmdcmr/img/svg/cappelle.svg',{addTo:true,changeSize:false,parent:cappelle});
    $(cappelle).animate({svgTransform:"translate(763,-15)"});


    var goeree = svg.group(rijnmond,{class:'gemeente Goeree-Overflakkee'})
    svg.load('http://dcmr-site.hrprojecten.net/bundles/rythmdcmr/img/svg/goeree.svg',{addTo:true,changeSize:false,parent:goeree});
     $(goeree).animate({svgTransform:"translate(150,300)"});

    var hellevoetsluis = svg.group(rijnmond,{class:'gemeente Hellevoetsluis'})
    svg.load('http://dcmr-site.hrprojecten.net/bundles/rythmdcmr/img/svg/hellevoetsluis.svg',{addTo:true,changeSize:false,parent:hellevoetsluis});
   $(hellevoetsluis).animate({svgTransform:"translate(155,160)"});

   var krimpen = svg.group(rijnmond,{class:'gemeente Krimpen-aan-den-ijsel'})
    svg.load('http://dcmr-site.hrprojecten.net/bundles/rythmdcmr/img/svg/krimpen.svg',{addTo:true,changeSize:false,parent:krimpen});
      $(krimpen).animate({svgTransform:"translate(795,23)"});

    var lasinger = svg.group(rijnmond,{class:'gemeente Lasinger'})
    svg.load('http://dcmr-site.hrprojecten.net/bundles/rythmdcmr/img/svg/lasinger.svg',{addTo:true,changeSize:false,parent:lasinger});
      $(lasinger).animate({svgTransform:"translate(664,-178)"});

    var maassluis = svg.group(rijnmond,{class:'gemeente Maassluis'})
    svg.load('http://dcmr-site.hrprojecten.net/bundles/rythmdcmr/img/svg/maassluis.svg',{addTo:true,changeSize:false,parent:maassluis});
      $(maassluis).animate({svgTransform:"translate(347,-8)"});


    var rest = svg.group(rijnmond,{class:'gemeente '})
    svg.load('http://dcmr-site.hrprojecten.net/bundles/rythmdcmr/img/svg/rest.svg',{addTo:true,changeSize:false,parent:rest});
    $(rest).animate({svgTransform:"translate(217,-13)"});

    var ridderkerk = svg.group(rijnmond,{class:'gemeente Ridderkerk'})
    svg.load('http://dcmr-site.hrprojecten.net/bundles/rythmdcmr/img/svg/ridderkerk.svg',{addTo:true,changeSize:false,parent:ridderkerk});
    $(ridderkerk).animate({svgTransform:"translate(790,127)"});

    var rotterdam = svg.group(rijnmond,{class:'gemeente Rotterdam'})
    svg.load('http://dcmr-site.hrprojecten.net/bundles/rythmdcmr/img/svg/rotterdam.svg',{addTo:true,changeSize:false,parent:rotterdam});
    $(rotterdam).animate({svgTransform:"translate(650,-40)"});

    var schiedam = svg.group(rijnmond,{class:'gemeente Schiedam'})
    svg.load('http://dcmr-site.hrprojecten.net/bundles/rythmdcmr/img/svg/schiedam.svg',{addTo:true,changeSize:false,parent:schiedam});
      $(schiedam).animate({svgTransform:"translate(508,-7)"});

    var spijkenisse = svg.group(rijnmond,{class:'gemeente Spijkenisse'})
    svg.load('http://dcmr-site.hrprojecten.net/bundles/rythmdcmr/img/svg/spijkenisse.svg',{addTo:true,changeSize:false,parent:spijkenisse});
    $(spijkenisse).animate({svgTransform:"translate(460,189)"});

    var vlaardingen = svg.group(rijnmond,{class:'gemeente Vlaardingen'})
    svg.load('http://dcmr-site.hrprojecten.net/bundles/rythmdcmr/img/svg/vlaardingen.svg',{addTo:true,changeSize:false,parent:vlaardingen});
      $(vlaardingen).animate({svgTransform:"translate(447,12)"});

    var westvoorne = svg.group(rijnmond,{class:'gemeente Westvoorne'})
    svg.load('http://dcmr-site.hrprojecten.net/bundles/rythmdcmr/img/svg/westvoorne.svg',{addTo:true,changeSize:false,parent:westvoorne});
    $(westvoorne).animate({svgTransform:"translate(117,74)"},function(){
        $('.gemeente').each(function(index){
            var text = svg.text(textgroep,$(this).attr('class').split(' ')[1],
            {fontFamily: 'Verdana',fontWeight:'Bold', fontSize: '20', fill:
            '#19660C',transform:$(this).attr('transform'),class:'text '+$(this).attr("class").split(" ")[1]+''});
        });
    });
     var textgroep = svg.group(rijnmond,{id:'titles'});
     $(textgroep).animate({svgTransform:"translate(75,180)"});
   $('#titles text').animate({svgTransform:"translate(2000,200)"});





}
function drawtable(svg){
    //TODO url veranderen.
svg.load('http://dcmr-site.hrprojecten.net/bundles/rythmdcmr/img/svg/thing.svg',{class:'UI',changeSize:false});
 next = svg.circle(20,20,40,{fill:"#D0D1FF",transform:"translate(502,650)"});
         svg.text("+",
            {fontFamily: 'Verdana',fontWeight:'Bold', fontSize: '50', fill:
            '#496BC1',transform:"translate(500,686)"});

 prev = svg.circle(20,20,40,{fill:"#D0D1FF",transform:"translate(420,650)"});
        svg.text("-",
            {fontFamily: 'Verdana',fontWeight:'Bold', fontSize: '50', fill:
            '#496BC1',transform:"translate(427,686)"});
 bar = svg.rect(40,43,210,60,{fill:"#EF4D4D",transform:"translate(170,520)"});
 pointer = svg.rect(40,43,20,25,{fill:"#D0D1FF",stroke:'#000000',transform:"translate(370,490)"});
 slider = svg.rect(50,50,10,80,{fill:"#3A9F4D",transform:"translate("+sliderX+",500)",class:'slider'});
 soundImage  = svg.group({id:'soundimage',transform:'scale(1,1) translate(450,300)'});
  textpoint = svg.text('Het geluids niveau bij u in de buurt ligt gemiddeld bij 60 decibel',
            {fontFamily: 'Verdana',fontWeight:'Bold', fontSize: '18', fill:
            '#496BC1',transform:"translate(120,530)",class:'textpoint'});
//TODO url veranderen.
  image = svg.image(soundImage,
  -200, -200, 400, 400,
  'http://dcmr-site.hrprojecten.net/bundles/rythmdcmr/img/svg/60.png',{class:'image',transform:'scale(1,1)'}) ;

   }

$(next).mousedown(function(){
    if(curImage< imageArray.length-1){
    if(parseInt($(bar).attr('width'))<535){

    $(bar).animate({svgWidth:parseInt($(bar).attr('width'))+54},400);
    $(slider).animate({svgTransform:"translate("+ (sliderX+54) +",500)"},400);
    $('.image').fadeOut(100);
    $('.image').animate({svgTransform:'scale(0,0)' },200,function(){
        //TODO hierbij de url veranderen voor plaatjes.
        $('.image').attr('href','http://dcmr-site.hrprojecten.net/bundles/rythmdcmr/img/svg/'+imageArray[curImage]+'.png');
    });
    $('.image').fadeIn(0);
    $('.image').animate({svgTransform:'scale(1,1) ' },200);
    sliderX+=54;
     curImage++;
    }
    }
    else{ $(bar).animate({svgWidth:535});}
});
$(prev).mousedown(function(){
 if(curImage >-1){
    if(parseInt($(bar).attr('width'))>0){
    $(bar).animate({svgWidth:parseInt($(bar).attr('width'))-54});
    $(slider).animate({svgTransform:"translate("+ (sliderX-54)+",500)"});
     $('.image').animate({svgTransform:'scale(0,0)' },200,function(){
        $('.image').attr('href','http://dcmr-site.hrprojecten.net/bundles/rythmdcmr/img/svg/'+imageArray[curImage]+'.png');
         $('.image').animate({svgTransform:'scale(1,1)' },200);
         sliderX-=54;
    });

     curImage--;
    }
    }
    else{
         $(bar).animate({svgWidth:0});
    }

});


$('#svg .gemeente').hover(function(){
  $("path[fill!='none']",this).animate({svgFill:"#92fa5f"},100);
  //alert($(this).attr('class').split(' ')[1]);
 var chosenText = $(this).attr('class').split(' ')[1];
 var hovered = "."+chosenText;
 $(hovered).animate({svgFill:"#000"});
},function(){
  $("path[fill!='none']",this).animate({svgFill:"#65CB33"},100);
  var chosenText = $(this).attr('class').split(' ')[1];
 var hovered = "."+chosenText;
 $(hovered).animate({svgFill:"#19660C"});
});
$('#svg .gemeente').click(function(){
var clicked =$(this).attr('class').split(' ')[1];
window.location.href = "http://dcmr-site.hrprojecten.net/locatie/"+clicked.toLowerCase();
});

    setTimeout(function(){
        $(slider).appendTo("#svg2 svg");

    },500);

//$(slider).mousedown(function(e){
//    cursorX = e.pageX-100;
//
//    $("svg").mousemove(function(event){
//
//        moved = cursorX - event.pageX;
//        alert(event.pageX);
//        alert(e.pageX);
//        sliderX= sliderX - moved;
//        $(slider).animate({svgTransform:"translate("+0+",500)"},0);
//       cursorX = event.pageX;
//
// });
//
//});

$(window).resize(function(){
  canvasWP = ($(window).width()/screen.width)-0.2;
     $("#chart").animate({svgTransform:"scale("+canvasWP+","+canvasWP+"),translate(0,150)"},0);

});

});

